const mqtt = require('mqtt');

const client = mqtt.connect('mqtts://broker.layfront.com:8883', {
  username: 'admin',
  password: '1q2w3e4r'
});

client.on('connect', () => {
  console.log('Connected to MQTT broker');
  client.subscribe('layfront/virtual-battery/sensing');
});

client.on('message', (topic, message) => {
  console.log(`Received message on topic "${topic}": ${message}`);
});
