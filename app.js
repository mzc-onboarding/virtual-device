const mqtt = require('mqtt');

const timeInterval = 1000; // 1 second
const chargePower = 50; // watts
const devicePower = 5; // watts
const mqttServer = 'broker.layfront.com';
const mqttPort = 8883;
const serialNumber = '1234567890ABCDEFGHIJ'; // 20 characters


const battery = {
  capacity: 5000, // mAh
  currentCharge: 5000, // mAh
  voltage: 3.3, // volts
  charging: false,
  health: true
};

let chargeDischargeCycleCounter = 0;

const client = mqtt.connect(`mqtts://${mqttServer}:${mqttPort}`, {
  username: 'admin',
  password: '1q2w3e4r'
});

client.on('connect', () => {
  console.log('Connected to MQTT server.');
});

setInterval(() => {
  let changeInCharge = 0;
  if (battery.charging) {
    changeInCharge = chargePower * timeInterval / (battery.voltage * 1000);
  } else if (battery.currentCharge / battery.capacity < 0.1) {
    battery.charging = true;
    chargeDischargeCycleCounter++;
  } else {
    changeInCharge = -devicePower * timeInterval / (battery.voltage * 1000);
  }

  battery.currentCharge += changeInCharge;
  if (battery.currentCharge >= battery.capacity) {
    battery.currentCharge = battery.capacity;
    battery.charging = false;
    chargeDischargeCycleCounter++;
  }

  const percentageCharge = (100 * battery.currentCharge / battery.capacity).toFixed(2);
  const remainingLife = (battery.currentCharge / devicePower).toFixed(2);
  const currentTime = new Date().toLocaleTimeString();
  battery.chargeRate = battery.charging ? chargePower / battery.voltage : 0;
  battery.current = (Math.abs(battery.charging
  ? battery.chargeRate / battery.voltage
  : devicePower / battery.voltage)).toFixed(2);
  battery.health = chargeDischargeCycleCounter < 1000;
  battery.temperature = Math.random() * (40 - 10) + 10;

  const data = {
    serialNumber,
    percentageCharge,
    remainingLife,
    currentTime,
    ...battery
  };

  delete data.timeRemaining;

  client.publish('layfront/virtual-battery/metric', JSON.stringify(data));
  client.publish('layfront/virtual-battery/sensing', JSON.stringify(data));
  console.log(`Published data to topic "layfront/virtual-battery/metric": ${JSON.stringify(data)}`);
}, timeInterval);
